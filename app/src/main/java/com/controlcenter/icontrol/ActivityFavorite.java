package com.controlcenter.icontrol;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.controlcenter.icontrol.othor.Constant;
import com.controlcenter.icontrol.othor.SharedPrefControl;

public class ActivityFavorite extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout rl_1, rl_2, rl_3, rl_4;
    private ImageView img_1, img_2, img_3, img_4;
    private SharedPrefControl sharedPrefControl;
    private int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        sharedPrefControl = new SharedPrefControl(this);

        rl_1 = findViewById(R.id.rl_1);
        rl_2 = findViewById(R.id.rl_2);
        rl_3 = findViewById(R.id.rl_3);
        rl_4 = findViewById(R.id.rl_4);
        img_1 = findViewById(R.id.img_1);
        img_2 = findViewById(R.id.img_2);
        img_3 = findViewById(R.id.img_3);
        img_4 = findViewById(R.id.img_4);

        rl_1.setOnClickListener(this);
        rl_2.setOnClickListener(this);
        rl_3.setOnClickListener(this);
        rl_4.setOnClickListener(this);

        setIcon1();

        setIcon2();

        setIcon3();

        setIcon4();
    }

    private void setIcon4() {
        try {
            Drawable img = getPackageManager().getApplicationIcon(sharedPrefControl.getItemAppDataMore4().getmPacketName());
            img_4.setImageDrawable(img);
        } catch (Exception e) {
            img_4.setImageResource(R.drawable.ic_default);
            e.printStackTrace();
        }
    }

    private void setIcon3() {
        try {
            Drawable img = getPackageManager().getApplicationIcon(sharedPrefControl.getItemAppDataMore3().getmPacketName());
            img_3.setImageDrawable(img);
        } catch (Exception e) {
            img_3.setImageResource(R.drawable.ic_default);
            e.printStackTrace();
        }
    }

    private void setIcon2() {
        try {
            Drawable img = getPackageManager().getApplicationIcon(sharedPrefControl.getItemAppDataMore2().getmPacketName());
            img_2.setImageDrawable(img);
        } catch (Exception e) {
            img_2.setImageResource(R.drawable.ic_default);
            e.printStackTrace();
        }
    }

    private void setIcon1() {
        try {
            Drawable img = getPackageManager().getApplicationIcon(sharedPrefControl.getItemAppDataMore1().getmPacketName());
            img_1.setImageDrawable(img);
        } catch (Exception e) {
            img_1.setImageResource(R.drawable.ic_default);
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_1:
                index = 0;
                break;

            case R.id.rl_2:
                index = 1;
                break;

            case R.id.rl_3:
                index = 2;
                break;

            case R.id.rl_4:
                index = 3;
                break;

        }
        Intent i = new Intent(this, AddViewActivity.class);
        i.putExtra(Constant.EXTRA_POS, index);
        startActivityForResult(i, index);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 0) {
            if (resultCode == Activity.RESULT_OK) {
                setIcon1();
            }
        } else if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                setIcon2();
            }
        } else if (requestCode == 2) {
            if (resultCode == Activity.RESULT_OK) {
                setIcon3();
            }
        } else if (requestCode == 3) {
            if (resultCode == Activity.RESULT_OK) {
                setIcon4();
            }
        }
    }
}
