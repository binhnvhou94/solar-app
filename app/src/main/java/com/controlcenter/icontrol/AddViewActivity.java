package com.controlcenter.icontrol;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.controlcenter.icontrol.adapter.ApplicationFavoriteAdapter;
import com.controlcenter.icontrol.asynctask.LoadDataApplication;
import com.controlcenter.icontrol.object.ItemApplication;
import com.controlcenter.icontrol.othor.Constant;

import java.util.ArrayList;

public class AddViewActivity extends Activity {

    private ArrayList<ItemApplication> arrItemApp;
    private ApplicationFavoriteAdapter adapter;
    private ListView lv_item_add;
    private LoadDataApplication loadDataApp;

    private int extra = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_view);

        extra = getIntent().getIntExtra(Constant.EXTRA_POS, 0);

        lv_item_add = findViewById(R.id.lv_item_add);
        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

        arrItemApp = new ArrayList<>();

        loadDataApp = new LoadDataApplication(this, new LoadDataApplication.DataItemApp() {
            @Override
            public void loadData(final ArrayList<ItemApplication> arr) {
                arrItemApp.addAll(arr);
                adapter = new ApplicationFavoriteAdapter(AddViewActivity.this, arrItemApp, extra);
                lv_item_add.setAdapter(adapter);
            }
        });
        loadDataApp.execute();
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
