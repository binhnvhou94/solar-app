package com.controlcenter.icontrol;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.controlcenter.icontrol.othor.Constant;
import com.controlcenter.icontrol.othor.SharedPrefControl;
import com.controlcenter.icontrol.service.ControlService;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

public class HandleSettingActivity extends Activity implements DiscreteSeekBar.OnProgressChangeListener {

    private DiscreteSeekBar dsb_size, dsb_temperature, dsb_intensity;
    private SharedPrefControl sharedPrefControl;
    private ImageView cimvtemperature, cimvintensity;
    private TextView txttemperature, txtintensity;

    private RadioGroup rg_position;
    private RadioButton rd_left, rd_right, rd_bottom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handle_setting);

        sharedPrefControl = new SharedPrefControl(HandleSettingActivity.this);

        findViewById(R.id.img_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        initView();
        initData();
    }

    private void initData() {
        //size
        dsb_size.setProgress(sharedPrefControl.getSizeView());
        dsb_size.setOnProgressChangeListener(this);

        //dsb_temperature
        dsb_temperature.setOnProgressChangeListener(this);
        dsb_temperature.setProgress(sharedPrefControl.getTemperature());

        //dsb_intensity
        dsb_intensity.setOnProgressChangeListener(this);
        dsb_intensity.setProgress(sharedPrefControl.getIntensity());

        int color = getcolor(sharedPrefControl.getTemperature());
        String hex = String.format("%02x%02x%02x", Color.red(color), Color.green(color), Color.blue(color));
        int colorid = Color.parseColor("#" + hex);
        cimvtemperature.setBackgroundColor(colorid);
        cimvintensity.setBackgroundColor(colorid);
    }

    private void initView() {

        rg_position = findViewById(R.id.rg_position);
        rd_left = findViewById(R.id.rd_left);
        rd_right = findViewById(R.id.rd_right);
        rd_bottom = findViewById(R.id.rd_bottom);

        switch (sharedPrefControl.getIndexPosition()) {
            case 0:
                rd_left.setChecked(true);
                break;

            case 1:
                rd_right.setChecked(true);
                break;

            case 2:
                rd_bottom.setChecked(true);
                break;
        }

        rg_position.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int id) {
                int i = 0;
                switch (id) {
                    case R.id.rd_left:
                        i = 0;
                        break;

                    case R.id.rd_right:
                        i = 1;
                        break;

                    case R.id.rd_bottom:
                        i = 2;
                        break;
                }
                sharedPrefControl.setIndexPosition(i);
                if (Constant.isMyServiceRunning(HandleSettingActivity.this, ControlService.class)) {
                    Intent intent = new Intent(HandleSettingActivity.this, ControlService.class);
                    intent.putExtra(Constant.CHANGE_POSITION, Constant.CHANGE_POSITION);
                    startService(intent);
                }
            }
        });

        dsb_size = findViewById(R.id.dsb_size);
        dsb_temperature = findViewById(R.id.dsb_temperature);
        dsb_intensity = findViewById(R.id.dsb_intensity);

        cimvtemperature = findViewById(R.id.profile_image_temperature);
        cimvintensity = findViewById(R.id.profile_image_intensity);

        txtintensity = findViewById(R.id.txt_intensity);

        txttemperature = findViewById(R.id.txt_temperature);

        //size
        dsb_size.setMax(200);
        dsb_size.setMin(100);

        //color
        dsb_temperature.setMax(100);
        dsb_temperature.setMin(0);

        //intensity
        dsb_intensity.setMax(100);
        dsb_intensity.setMin(1);

    }


    @Override
    public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.dsb_size:
                sharedPrefControl.setSizeView(value);
                if (Constant.isMyServiceRunning(HandleSettingActivity.this, ControlService.class)) {
                    Intent intent = new Intent(HandleSettingActivity.this, ControlService.class);
                    intent.putExtra(Constant.CHANGE_SIZE, Constant.CHANGE_SIZE);
                    startService(intent);
                }
                break;

            case R.id.dsb_temperature:
                int color = getcolor(value);
                String hex = String.format("%02x%02x%02x", Color.red(color), Color.green(color), Color.blue(color));
                int colorid = Color.parseColor("#" + hex);
                cimvtemperature.setBackgroundColor(colorid);
                cimvintensity.setBackgroundColor(colorid);

                sharedPrefControl.setTemperature(value);

                if (value <= 60) {
                    txttemperature.setText("#" + hex + " " + getString(R.string.rela));
                } else {
                    txttemperature.setText("#" + hex + " " + getString(R.string.Ene));
                }

                if (Constant.isMyServiceRunning(HandleSettingActivity.this, ControlService.class)) {
                    Intent intent = new Intent(HandleSettingActivity.this, ControlService.class);
                    intent.putExtra(Constant.TEMPERATURE_VIEW, Constant.TEMPERATURE_VIEW);
                    startService(intent);
                }

                break;

            case R.id.dsb_intensity:
                float p = (float) (100 - value) / 100.0f;
                cimvintensity.setAlpha(p);
                txtintensity.setText(value + "%");
                sharedPrefControl.setIntensity(value);

                if (Constant.isMyServiceRunning(HandleSettingActivity.this, ControlService.class)) {
                    Intent intent = new Intent(HandleSettingActivity.this, ControlService.class);
                    intent.putExtra(Constant.INTENSITY_VIEW, Constant.INTENSITY_VIEW);
                    startService(intent);
                }

                break;
        }
    }

    private int getcolor(float percent) {
        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.bitmkapcolor);
        float pixel = ((icon.getWidth() * percent)) / 100;
        if (pixel >= icon.getWidth()) {
            pixel = icon.getWidth() - 1;
        }
        int xxx = icon.getPixel((int) pixel, ((icon.getHeight() / 2)));
        return xxx;
    }

    @Override
    public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

    }
}
