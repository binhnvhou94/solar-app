package com.controlcenter.icontrol;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import com.controlcenter.icontrol.othor.Constant;
import com.controlcenter.icontrol.othor.SharedPrefControl;
import com.controlcenter.icontrol.service.ControlService;


public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private ToggleButton tg_control;
    private SwitchCompat sw_vibration, sw_touch, sw_noti;
    private RelativeLayout rl_size, rl_color, rl_pos, rl_music_control, rl_bgr, rl_vibration, rl_touch, rl_noti, rl_favo;
    private SharedPrefControl sharedPrefControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_ad);

        sharedPrefControl = new SharedPrefControl(this);

        initView();

        initData();

    }

    private void initData() {
        //set Switch Control
        tg_control.setChecked(sharedPrefControl.getStatusControl());
        //set SW vibration
        sw_vibration.setChecked(sharedPrefControl.getStatusVibration());
        //set SW Touch to Open
        sw_touch.setChecked(sharedPrefControl.getStatusTouch());
        //set Sw show Noti
        sw_noti.setChecked(sharedPrefControl.getStatusShowNoti());
    }

    private void initView() {
        tg_control = findViewById(R.id.tg_control);
        rl_favo = findViewById(R.id.rl_favo);
        sw_vibration = findViewById(R.id.sw_vibration);
        sw_touch = findViewById(R.id.sw_touch);
        sw_noti = findViewById(R.id.sw_noti);
        rl_size = findViewById(R.id.rl_size);
        rl_color = findViewById(R.id.rl_color);
        rl_pos = findViewById(R.id.rl_pos);
        rl_music_control = findViewById(R.id.rl_music_control);
        rl_bgr = findViewById(R.id.rl_bgr);
        rl_vibration = findViewById(R.id.rl_vibration);
        rl_touch = findViewById(R.id.rl_touch);
        rl_noti = findViewById(R.id.rl_noti);

        tg_control.setOnCheckedChangeListener(this);
        sw_vibration.setOnCheckedChangeListener(this);
        sw_touch.setOnCheckedChangeListener(this);
        sw_noti.setOnCheckedChangeListener(this);

        rl_size.setOnClickListener(this);
        rl_color.setOnClickListener(this);
        rl_pos.setOnClickListener(this);
        rl_music_control.setOnClickListener(this);
        rl_bgr.setOnClickListener(this);
        rl_vibration.setOnClickListener(this);
        rl_touch.setOnClickListener(this);
        rl_noti.setOnClickListener(this);
        rl_favo.setOnClickListener(this);
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        switch (compoundButton.getId()) {
            case R.id.tg_control:
                checkControlService(b);
                break;
            case R.id.sw_noti:
                sharedPrefControl.setStatusShowNoti(b);
                if (Constant.isMyServiceRunning(MainActivity.this, ControlService.class)) {
                    Intent intent = new Intent(MainActivity.this, ControlService.class);
                    intent.putExtra(Constant.PUT_SHOW_NOTI, b);
                    intent.putExtra(Constant.ACCESS_SHOW_NOTI, Constant.ACCESS_SHOW_NOTI);
                    startService(intent);
                }
                break;

            case R.id.sw_touch:
                sharedPrefControl.setStatusTouch(b);
                break;

            case R.id.sw_vibration:
                sharedPrefControl.setStatusVibration(b);
                break;
        }
    }

    private void checkControlService(boolean check) {
        sharedPrefControl.setStatusControl(check);
        if (check) {
            if (!checkNotificationAccessPermission()) {
                showDialog();
            }
            if (!Constant.isMyServiceRunning(MainActivity.this, ControlService.class))
                startService(new Intent(MainActivity.this, ControlService.class));
        } else {
            stopService(new Intent(MainActivity.this, ControlService.class));
        }
    }

    private boolean checkNotificationAccessPermission() {
        if (Settings.Secure.getString(this.getContentResolver(), "enabled_notification_listeners") != null)
            if (Settings.Secure.getString(this.getContentResolver(), "enabled_notification_listeners").contains(getApplicationContext().getPackageName())) {
                return true;
            } else {
                return false;
            }
        return false;
    }

    private void showDialog() {
        final AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(MainActivity.this);
        }
        builder.setTitle("Notification Listener Service")
                .setMessage("For the app to work you need to enable the Notification Listener Service. Enable it now?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.rl_size:
                startActivity(new Intent(MainActivity.this, HandleSettingActivity.class));
                break;

            case R.id.rl_color:
                startActivity(new Intent(MainActivity.this, HandleSettingActivity.class));
                break;

            case R.id.rl_pos:
                startActivity(new Intent(MainActivity.this, HandleSettingActivity.class));
                break;

            case R.id.rl_music_control:
                Intent intent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;

            case R.id.rl_bgr:
                showSortDialog();
                break;

            case R.id.rl_vibration:
                sw_vibration.setChecked(!sharedPrefControl.getStatusVibration());
                break;

            case R.id.rl_touch:
                sw_touch.setChecked(!sharedPrefControl.getStatusTouch());
                break;

            case R.id.rl_noti:
                sw_noti.setChecked(!sharedPrefControl.getStatusShowNoti());
                break;

            case R.id.rl_favo:
                startActivity(new Intent(MainActivity.this, ActivityFavorite.class));
                break;
        }
    }

    private void showSortDialog() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_background);
        RadioButton rd_trans = dialog.findViewById(R.id.rd_trans);
        RadioButton rd_wallpaper = dialog.findViewById(R.id.rd_wallpaper);
        RadioButton rd_default = dialog.findViewById(R.id.rd_default);
        RadioButton rd_blur = dialog.findViewById(R.id.rd_blur);
        switch (sharedPrefControl.getIndexBackground()) {
            case 0:
                rd_trans.setChecked(true);
                break;

            case 1:
                rd_wallpaper.setChecked(true);
                break;
            case 2:
                rd_default.setChecked(true);
                break;
            case 3:
                rd_blur.setChecked(true);
                break;
        }

        RadioGroup rg_bgr = dialog.findViewById(R.id.rg_bgr);
        rg_bgr.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                switch (i) {
                    case R.id.rd_trans:
                        sharedPrefControl.setIndexBackground(0);
                        break;
                    case R.id.rd_wallpaper:
                        sharedPrefControl.setIndexBackground(1);
                        break;
                    case R.id.rd_default:
                        sharedPrefControl.setIndexBackground(2);
                        break;
                    case R.id.rd_blur:
                        sharedPrefControl.setIndexBackground(3);
                        break;
                }
                dialog.dismiss();
            }
        });

        dialog.show();

    }
}
