package com.controlcenter.icontrol.adapter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.controlcenter.icontrol.R;
import com.controlcenter.icontrol.object.ItemApplication;
import com.controlcenter.icontrol.othor.Constant;
import com.controlcenter.icontrol.othor.SharedPrefControl;

import java.util.ArrayList;


public class ApplicationFavoriteAdapter extends BaseAdapter {
    private ArrayList<ItemApplication> arrItem;
    private LayoutInflater inflater;
    private int selectedPosition = -1;
    private Context mContext;
    private int extra = 0;
    private SharedPrefControl sharedPrefControl;
    private ItemApplication obj;

    public ApplicationFavoriteAdapter(Context context, ArrayList<ItemApplication> arrItem, int extra) {
        this.mContext = context;
        this.arrItem = new ArrayList<>();
        this.arrItem.add(new ItemApplication(false, Constant.NON_APP, Constant.NON_APP, 0));
        this.arrItem.addAll(arrItem);
        inflater = LayoutInflater.from(context);
        this.extra = extra;
        this.sharedPrefControl = new SharedPrefControl(mContext);


        if (extra == 0) {
            obj = sharedPrefControl.getItemAppDataMore1();
        } else if (extra == 1) {
            obj = sharedPrefControl.getItemAppDataMore2();
        } else if (extra == 2) {
            obj = sharedPrefControl.getItemAppDataMore3();
        } else if (extra == 3) {
            obj = sharedPrefControl.getItemAppDataMore4();
        }
        if (obj != null) {
            for (ItemApplication item : this.arrItem) {
                if (item.getmPacketName().equals(obj.getmPacketName())) {
                    item.setmCheck(true);
                    break;
                }
            }
        } else {
            this.arrItem.get(0).setmCheck(true);
        }
    }

    @Override
    public int getCount() {
        return arrItem.size();
    }

    @Override
    public Object getItem(int i) {
        return arrItem.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null) {
            viewHolder = new ViewHolder();

            //inflate the layout on basis of boolean
            view = inflater.inflate(R.layout.item_add_view, viewGroup, false);

            viewHolder.img_icon = view.findViewById(R.id.img_icon);
            viewHolder.cb_item = view.findViewById(R.id.cb_item);

            view.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) view.getTag();
        ItemApplication item = arrItem.get(i);
        viewHolder.cb_item.setChecked(item.ismCheck());
        if (i == 0) {
            viewHolder.cb_item.setText(Constant.NON_APP_TEXT);
            viewHolder.img_icon.setImageResource(R.drawable.ic_non);
        } else {
            viewHolder.cb_item.setText(item.getmAppName());
            try {
                Drawable img = mContext.getPackageManager().getApplicationIcon(arrItem.get(i).getmPacketName());
                viewHolder.img_icon.setImageDrawable(img);

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        //check the radio button if both position and selectedPosition matches
        viewHolder.cb_item.setChecked(i == selectedPosition);

        //Set the position tag to both radio button and label
        viewHolder.cb_item.setTag(i);
        viewHolder.img_icon.setTag(i);

        viewHolder.cb_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemCheckChanged(v);
            }
        });

        viewHolder.img_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemCheckChanged(v);
            }


        });
        return view;
    }

    //On selecting any view set the current position to selectedPositon and notify adapter
    private void itemCheckChanged(View v) {
        selectedPosition = (Integer) v.getTag();
        if (extra == 0) {
            sharedPrefControl.setItemAppDataMore1(arrItem.get(selectedPosition));
        } else if (extra == 1) {
            sharedPrefControl.setItemAppDataMore2(arrItem.get(selectedPosition));
        } else if (extra == 2) {
            sharedPrefControl.setItemAppDataMore3(arrItem.get(selectedPosition));
        } else if (extra == 3) {
            sharedPrefControl.setItemAppDataMore4(arrItem.get(selectedPosition));
        }
        notifyDataSetChanged();
    }

    private class ViewHolder {
        private ImageView img_icon;
        private RadioButton cb_item;
    }

}

