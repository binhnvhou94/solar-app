package com.controlcenter.icontrol.asynctask;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;

import com.controlcenter.icontrol.object.ItemApplication;
import com.controlcenter.icontrol.othor.SharedPrefControl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cheng Lee on 9/28/2017.
 */

public class LoadDataApplication extends AsyncTask<Void, Void, ArrayList<ItemApplication>> {

    private Context mContext;
    private SharedPrefControl sharedPrefControl;
    private DataItemApp dataItemApp;

    public LoadDataApplication(Context mContext, DataItemApp dataItemApp) {
        this.mContext = mContext;
        this.dataItemApp = dataItemApp;
        sharedPrefControl = new SharedPrefControl(mContext);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //start loading
    }

    @Override
    protected ArrayList<ItemApplication> doInBackground(Void... voids) {


        ArrayList<ItemApplication> arrItemApp = new ArrayList<>();
        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> pkgAppsList = mContext.getPackageManager().queryIntentActivities(mainIntent, 0);

        for (ResolveInfo itemRes : pkgAppsList) {
            boolean check = false;
            String pkn = itemRes.activityInfo.packageName;
            String appName = "";
            try {
                PackageManager packageManager = mContext.getApplicationContext().getPackageManager();
                appName = (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(pkn, PackageManager.GET_META_DATA));
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            arrItemApp.add(new ItemApplication(check, pkn, appName, 0));
        }
        return arrItemApp;
    }

    @Override
    protected void onPostExecute(ArrayList<ItemApplication> arr) {
        super.onPostExecute(arr);
        dataItemApp.loadData(arr);

        //hide loading
    }

    public interface DataItemApp {
        void loadData(ArrayList<ItemApplication> arr);
    }

}
