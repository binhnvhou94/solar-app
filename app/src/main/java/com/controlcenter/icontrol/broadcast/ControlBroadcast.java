package com.controlcenter.icontrol.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.controlcenter.icontrol.othor.Constant;
import com.controlcenter.icontrol.othor.SharedPrefControl;
import com.controlcenter.icontrol.service.ControlService;

import static android.content.ContentValues.TAG;

/**
 * Created by Cheng Lee on 9/25/2017.
 */

public class ControlBroadcast extends BroadcastReceiver {
    private SharedPrefControl mSharedPrefControl;

    @Override
    public void onReceive(Context context, Intent intent) {

        mSharedPrefControl = new SharedPrefControl(context);
        try {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                showService(intent, context, false);
            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                showService(intent, context, true);
            } else if ((intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED))) {
                showService(intent, context, true);
            }
        } catch (Exception e) {
            Log.i(TAG, e.toString());
        }
    }

    private void showService(Intent mIntent, Context mContext, boolean servicecheck) {
        if (mSharedPrefControl.getStatusControl()) {
            mIntent.setClass(mContext, ControlService.class);
            mIntent.putExtra(Constant.CHECK_SERVICE, servicecheck);
            mContext.startService(mIntent);
        }
    }
}
