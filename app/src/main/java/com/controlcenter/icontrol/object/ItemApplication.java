package com.controlcenter.icontrol.object;

/**
 * Created by Cheng Lee on 9/28/2017.
 */

public class ItemApplication {
    private boolean mCheck;
    private String mPacketName;
    private String mAppName;
    private int mIndex;

    public ItemApplication(boolean mCheck, String mPacketName,  String mAppName, int mIndex) {
        this.mCheck = mCheck;
        this.mPacketName = mPacketName;
        this.mAppName = mAppName;
        this.mIndex = mIndex;
    }

    public boolean ismCheck() {
        return mCheck;
    }

    public void setmCheck(boolean mCheck) {
        this.mCheck = mCheck;
    }

    public String getmPacketName() {
        return mPacketName;
    }

    public void setmPacketName(String mPacketName) {
        this.mPacketName = mPacketName;
    }

    public String getmAppName() {
        return mAppName;
    }

    public void setmAppName(String mAppName) {
        this.mAppName = mAppName;
    }

    public int getmIndex() {
        return mIndex;
    }

    public void setmIndex(int mIndex) {
        this.mIndex = mIndex;
    }
}
