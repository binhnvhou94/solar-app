package com.controlcenter.icontrol.othor;

import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.controlcenter.icontrol.R;
import com.controlcenter.icontrol.widgets.VerticalSeekBar;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static android.content.ContentValues.TAG;

/**
 * Created by Cheng Lee on 9/25/2017.
 */

public class ApplicationMethor {

    private int maxVolume;
    private WifiManager wifiManager;
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private NotificationManager mNotificationManager;
    private boolean isEnabledAirPlane;
    private AudioManager mAudioManager = null;

    private final String COMMAND_FLIGHT_MODE_1 = "settings put global airplane_mode_on";
    private final String COMMAND_FLIGHT_MODE_2 = "am broadcast -a android.intent.action.AIRPLANE_MODE --ez state";

    private Context mContext;

    //create flash
    //flash
    private Camera camera;
    private boolean isFlashOn;
    private boolean hasFlash;
    Camera.Parameters params;


    public ApplicationMethor(Context mContext) {
        this.mContext = mContext;
        wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        try {
            mBluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        } catch (Exception e) {
            Toast.makeText(mContext, "No Bluetooth!", Toast.LENGTH_SHORT).show();
        }

        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        isEnabledAirPlane = Settings.System.getInt(mContext.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) == 1;
        mAudioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
    }

    public void setFlightMode(Context context) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            // API 17 onwards.
            if (isRooted()) {
                int enabled = isFlightModeEnabled(context) ? 0 : 1;
                // Set Airplane / Flight mode using su commands.
                String command = COMMAND_FLIGHT_MODE_1 + " " + enabled;
                executeCommandWithoutWait(context, "-c", command);
                command = COMMAND_FLIGHT_MODE_2 + " " + enabled;
                executeCommandWithoutWait(context, "-c", command);
            } else {
                try {
                    // No root permission, just show Airplane / Flight mode setting screen.
                    Intent intent = new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Log.e("Binh", "Setting screen not found due to: " + e.fillInStackTrace());
                }
            }
        } else {
            // API 16 and earlier.
            boolean enabled = isFlightModeEnabled(context);
            Settings.System.putInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, enabled ? 0 : 1);
            Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
            intent.putExtra("state", !enabled);
            context.sendBroadcast(intent);
        }
    }

    private void executeCommandWithoutWait(Context context, String option, String command) {
        boolean success = false;
        String su = "su";
        for (int i = 0; i < 3; i++) {
            // "su" command executed successfully.
            if (success) {
                // Stop executing alternative su commands below.
                break;
            }
            if (i == 1) {
                su = "/system/xbin/su";
            } else if (i == 2) {
                su = "/system/bin/su";
            }
            try {
                // execute command
                Runtime.getRuntime().exec(new String[]{su, option, command});
            } catch (IOException e) {
                Log.e(TAG, "su command has failed due to: " + e.fillInStackTrace());
                Intent intent = new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        }
    }

    private boolean isFlightModeEnabled(Context context) {
        boolean mode = false;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            // API 17 onwards
            mode = Settings.Global.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) == 1;
        } else {
            // API 16 and earlier.
            mode = Settings.System.getInt(context.getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) == 1;
        }
        return mode;
    }

    public static boolean isRooted() {

        // get from build info
        String buildTags = android.os.Build.TAGS;
        if (buildTags != null && buildTags.contains("test-keys")) {
            return true;
        }

        // check if /system/app/Superuser.apk is present
        try {
            File file = new File("/system/app/Superuser.apk");
            if (file.exists()) {
                return true;
            }
        } catch (Exception e1) {
            // ignore
        }

        // try executing commands
        return canExecuteCommand("/system/xbin/which su")
                || canExecuteCommand("/system/bin/which su") || canExecuteCommand("which su");
    }

    // executes a command on the system
    private static boolean canExecuteCommand(String command) {
        boolean executedSuccesfully;
        try {
            Runtime.getRuntime().exec(command);
            executedSuccesfully = true;
        } catch (Exception e) {
            executedSuccesfully = false;
        }

        return executedSuccesfully;
    }

    public void setAirMode(ImageView imgbairPlane, Context context) {
        if (isFlightModeEnabled(context)) {
            imgbairPlane.setImageResource(R.drawable.airplane_mode_on);
        } else {
            imgbairPlane.setImageResource(R.drawable.airplane_mode_off);
        }
    }

    public void setMobileDataEnabled(boolean enabled) {
        try {
            final ConnectivityManager conman = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            final Class conmanClass = Class.forName(conman.getClass().getName());
            final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
            iConnectivityManagerField.setAccessible(true);
            final Object iConnectivityManager = iConnectivityManagerField.get(conman);
            final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
            final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            setMobileDataEnabledMethod.setAccessible(true);

            setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //wifi
    private Boolean checkWifi() {
        return wifiManager.isWifiEnabled();
    }

    public void getStatusWifi(ImageView ibtnWifi) {
        if (!checkWifi()) {
            ibtnWifi.setImageResource(R.drawable.wifi_off);
        } else {
            ibtnWifi.setImageResource(R.drawable.wifi_on);
        }
    }

    public void setWifi(Boolean ktwifi) {
        wifiManager.setWifiEnabled(ktwifi);
    }

    public void wifisettingMode(ImageView img) {
        if (checkWifi()) {
            setWifi(false);
            img.setImageResource(R.drawable.wifi_off);
        } else {
            setWifi(true);
            img.setImageResource(R.drawable.wifi_on);
        }
    }

    //bluetooth
    // bluetoothmode
    private Boolean checBlueTooth() {
        try {
            return mBluetoothAdapter.isEnabled();
        } catch (Exception e) {
            return false;
        }
    }

    public void OnBlueTooth(ImageView img) {
        try {
            mBluetoothAdapter.enable();
            img.setImageResource(R.drawable.bluetooth_on);
        } catch (Exception e){
            Toast.makeText(mContext, "Enable Bluetooth Fail, Try again!", Toast.LENGTH_SHORT).show();
            img.setImageResource(R.drawable.bluetooth_off);
        }

    }

    public void OffBlueTooth() {
        mBluetoothAdapter.disable();
    }

    public void setStatusBluetooth(ImageView img) {
        if (checBlueTooth()) {
            img.setImageResource(R.drawable.bluetooth_on);
        } else {
            img.setImageResource(R.drawable.bluetooth_off);
        }
    }

    public void bluetoothSettingMode(ImageView img) {
        if (!checBlueTooth()) {
            OnBlueTooth(img);
        } else {
            OffBlueTooth();
            img.setImageResource(R.drawable.bluetooth_off);
        }
    }

    //setAutoOrientationEnabled
    private Boolean setAutoOrientationEnabled() {
        return android.provider.Settings.System.getInt(mContext.getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0) == 1;
    }

    public void getStatusOrientation(ImageView img) {
        if (setAutoOrientationEnabled()) {
            img.setImageResource(R.drawable.orientation_on);
        } else {
            img.setImageResource(R.drawable.orientation_off);
        }
    }

    private void turnOnRotation() {
        android.provider.Settings.System.putInt(mContext.getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 1);
    }

    private void turnOffRotation() {
        android.provider.Settings.System.putInt(mContext.getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0);
    }

    public void ScreenSettingOrtation(ImageView img) {
        try {
            if (!setAutoOrientationEnabled()) {
                turnOnRotation();
                img.setImageResource(R.drawable.orientation_on);
            } else {
                turnOffRotation();
                img.setImageResource(R.drawable.orientation_off);
            }
        } catch (Exception e) {
            Constant.showToastFail(mContext, "Setting Fail, Try Again!");
        }
    }

    //Bother
    public void getStatusBother(ImageView img) {
        switch (mAudioManager.getRingerMode()) {
            case AudioManager.RINGER_MODE_NORMAL:
                img.setImageResource(R.drawable.nightmode_off);
                break;
            case AudioManager.RINGER_MODE_SILENT:
                img.setImageResource(R.drawable.nightmode_on);
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                img.setImageResource(R.drawable.nightmode_off);
                break;
        }
    }

    public void setupBother(ImageView img) {

        switch (mAudioManager.getRingerMode()) {
            case AudioManager.RINGER_MODE_NORMAL:
                img.setImageResource(R.drawable.nightmode_on);
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                break;
            case AudioManager.RINGER_MODE_SILENT:
                img.setImageResource(R.drawable.nightmode_off);
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                img.setImageResource(R.drawable.nightmode_on);
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                break;
        }
    }

    //flash mode
    public void getCamera() {
        if (camera == null) {
            try {
                camera = Camera.open();
                params = camera.getParameters();
            } catch (RuntimeException e) {

            }
        }

    }

    public boolean checkStatusFlashLight() {
        boolean check = false;
        try {
            if (params.getFlashMode() == android.hardware.Camera.Parameters.FLASH_MODE_ON) {
                check = true;
            } else {
                check = false;
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            stopCamera();
            check = false;
        }
        return check;
    }

    public void getStatusFlashLight(ImageView img) {
        try {
            if (params.getFlashMode() == android.hardware.Camera.Parameters.FLASH_MODE_ON) {
                img.setImageResource(R.drawable.flash_on);
                img.setBackgroundResource(R.drawable.bgr_transparent_white);
            } else {
                img.setImageResource(R.drawable.flash_off);
                img.setBackgroundResource(R.drawable.bgr_transparent_black);
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            stopCamera();
            img.setImageResource(R.drawable.flash_off);
            img.setBackgroundResource(R.drawable.bgr_transparent_black);
        }
    }

    public Boolean checkFlashship() {
        return hasFlash = mContext.getApplicationContext().getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    public void checkflashsystem() {
        if (!checkFlashship()) {
            Constant.showToastFail(mContext, "No Flash!");
            return;
        }
    }

    public void checkflash(ImageView img) {
        if (isFlashOn) {
            // turn off flash
            turnOffFlash(img);
        } else {
            // turn on flash
            turnOnFlash(img);
        }
    }

    public void stopCamera() {
        try {
            if (camera != null) {
                camera.release();
                camera = null;
            }
        } catch (Exception e) {

        }
    }

    /*
    * Turning Off flash
    */
    private void turnOffFlash(ImageView img) {
        if (isFlashOn) {
            if (camera == null || params == null) {
                return;
            }
            // play sound

            params = camera.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            camera.setParameters(params);
            camera.stopPreview();
            isFlashOn = false;

            // changing button/switch image
            toggleButtonImage(img);
        }
    }

    private void toggleButtonImage(ImageView img) {
        if (isFlashOn) {
            img.setImageResource(R.drawable.flash_on);
            img.setBackgroundResource(R.drawable.bgr_transparent_white);
        } else {
            img.setImageResource(R.drawable.flash_off);
            img.setBackgroundResource(R.drawable.bgr_transparent_black);
        }
    }

    private void turnOnFlash(ImageView ibtnApp) {
        if (!isFlashOn) {
            if (camera == null || params == null) {
                return;
            }
            // play sound
            params = camera.getParameters();
            params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(params);
            camera.startPreview();
            isFlashOn = true;

            // changing button/switch image
            toggleButtonImage(ibtnApp);
        }
    }

    //data mobile 4G
    //the method below enables/disables mobile data depending on the Boolean 'enabled' parameter.
    private void setMobileDataEnabledBelowKITKAT(Context context, boolean enabled) {
        final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        Class conmanClass = null;
        try {
            conmanClass = Class.forName(conman.getClass().getName());
            final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
            iConnectivityManagerField.setAccessible(true);
            final Object iConnectivityManager = iConnectivityManagerField.get(conman);
            final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
            final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            setMobileDataEnabledMethod.setAccessible(true);
            setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    private void setMobileDataEnabled() {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
        mContext.startActivity(intent);
    }

    // below method returns true if mobile data is on and vice versa
    private boolean mobileDataEnabled() {
        boolean mobileDataEnabled = false; // Assume disabled
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            Class cmClass = Class.forName(cm.getClass().getName());
            Method method = cmClass.getDeclaredMethod("getMobileDataEnabled");
            method.setAccessible(true); // Make the method callable
            // get the setting for "mobile data"
            mobileDataEnabled = (Boolean) method.invoke(cm);
        } catch (Exception e) {
            // Some problem accessible private API
            // TODO do whatever error handling you want here
            Log.e("Binhnv", "BUG: " + e.getMessage());
        }
        return mobileDataEnabled;
    }

    public void checkMobileDataEnable(ImageView img) {
        if (mobileDataEnabled()) {
            img.setImageResource(R.drawable.datamobile_on);
        } else {
            img.setImageResource(R.drawable.datamobile_off);
        }
    }

    public void dataMobile() {
        if (!mobileDataEnabled()) {
            setMobileDataEnabled();
        } else {
            setMobileDataEnabled();
        }
    }

    //setup screen time out
    public void setupScreenTimeout(int time) {
        android.provider.Settings.System.putInt(mContext.getContentResolver(),
                Settings.System.SCREEN_OFF_TIMEOUT, time);
    }

    //light - brightness
    public void getProgressBarBrightness(VerticalSeekBar sb, ImageView img) {
        try {
            int brightness = android.provider.Settings.System.getInt(
                    mContext.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS);

            sb.setProgress(brightness);

            setupImageBrightness(img, brightness);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupImageBrightness(ImageView img, int brightness) {
        if (brightness <= 60) {
            img.setImageResource(R.drawable.brightness_lv1);
        } else if (brightness <= 130) {
            img.setImageResource(R.drawable.brightness_lv2);
        } else if (brightness <= 190) {
            img.setImageResource(R.drawable.brightness_lv3);
        } else {
            img.setImageResource(R.drawable.brightness_lv4);
        }
    }

    public void setAutoBrightness(TextView txtBrighr) {
        if (Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC == 1) {
            txtBrighr.setText(" ON");
        } else {
            txtBrighr.setText(" OFF");
        }
    }

    private void OnAutoBright() {
        Settings.System.putInt(mContext.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, 1);
    }

    private void OffAutoBright() {
        Settings.System.putInt(mContext.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS_MODE, Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
    }

    public void setBrightness(int br, ImageView img) {
        try {
            android.provider.Settings.System.putInt(mContext.getContentResolver(),
                    android.provider.Settings.System.SCREEN_BRIGHTNESS, br);
            setupImageBrightness(img, br);
        } catch (Exception e){

        }

    }

    public void settingAutoBrightness(TextView txtBrighr) {
        if (Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC == 1) {
            OffAutoBright();
            txtBrighr.setText(" OFF");
        } else {
            OnAutoBright();
            txtBrighr.setText(" ON");
        }
    }

    //volumn
    public void getSeekbarVolumn(VerticalSeekBar sb, ImageView img) {
        int curVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_RING);
        sb.setMax(maxVolume);
        sb.setProgress(curVolume);
        sb.setMax(maxVolume);
        sb.setProgress(curVolume);
        setupImageVolumn(img, curVolume);
    }

    private void setupImageVolumn(ImageView img, int curVolume) {
        if (curVolume == 0) {
            img.setImageResource(R.drawable.volumn_lv0);
        } else if (curVolume <= maxVolume / 4) {
            img.setImageResource(R.drawable.volumn_lv1);
        } else if (curVolume <= maxVolume / 2) {
            img.setImageResource(R.drawable.volumn_lv2);
        } else if (curVolume <= 3 * maxVolume / 4) {
            img.setImageResource(R.drawable.volumn_lv3);
        } else {
            img.setImageResource(R.drawable.volumn_lv4);
        }
    }

    public void setVolume(int vol, ImageView img) {
        mAudioManager.setStreamVolume(AudioManager.STREAM_RING, vol, AudioManager.FLAG_ALLOW_RINGER_MODES);
        setupImageVolumn(img, vol);
    }

}
