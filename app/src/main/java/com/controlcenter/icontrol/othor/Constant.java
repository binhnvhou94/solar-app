package com.controlcenter.icontrol.othor;

import android.app.ActivityManager;
import android.content.Context;
import android.widget.Toast;

public class Constant {

    // turn on/off service Control
    public static final String KEY_STATUS_CONTROL = "KEY_STATUS_CONTROL";
    public static final String CHECK_SERVICE = "CHECK_SERVICE";

    public static final String ITEM_APP_1 = "ITEM_APP_1";
    public static final String ITEM_APP_2 = "ITEM_APP_2";
    public static final String ITEM_APP_3 = "ITEM_APP_3";
    public static final String ITEM_APP_4 = "ITEM_APP_4";
    public static final String MUSIC_NAME = "MUSIC_NAME";
    public static final String SINGER_NAME = "SINGER_NAME";
    public static final String BITMAP_IMAGE = "BITMAP_IMAGE";
    public static final String STATUS_PLAY = "STATUS_PLAY";
    public static final String EXTRA_POS = "EXTRA_POS";
    public static final String NON_APP = "NON_APP";
    public static final String NON_APP_TEXT = "NON APP";
    public static final String STATUS_VIBRATION = "STATUS_VIBRATION";
    public static final String STATUS_TOUCH = "STATUS_TOUCH";
    public static final String STATUS_SHOW_NOTI = "STATUS_SHOW_NOTI";
    public static final String INDEX_BACKGROUND = "INDEX_BACKGROUND";
    public static final String PUT_SHOW_NOTI = "PUT_SHOW_NOTI";
    public static final String ACCESS_SHOW_NOTI = "ACCESS_SHOW_NOTI";
    public static final String SIZE_VIEW = "SIZE_VIEW";
    public static final String CHANGE_SIZE = "CHANGE_SIZE";
    public static final String TEMPERATURE = "TEMPERATURE";
    public static final String TEMPERATURE_VIEW = "TEMPERATURE_VIEW";
    public static final String INTENSITY_VIEW = "INTENSITY_VIEW";
    public static final String INTENSITY = "INTENSITY";
    public static final String INDEX_POSTION = "INDEX_POSTION";
    public static final String CHANGE_POSITION = "CHANGE_POSITION";


    //    --------------------     //
    public static String NAME_SHAREDPREF = "NAME_SHAREDPREF";

    public static boolean isMyServiceRunning(Context mContext, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void showToastFail(Context mContext, String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }
}
