package com.controlcenter.icontrol.othor;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.controlcenter.icontrol.R;
import com.controlcenter.icontrol.object.ItemApplication;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;

/**
 * Created by Cheng Lee on 9/25/2017.
 */

public class SharedPrefControl {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private Context mContext;

    public SharedPrefControl(Context mContext) {
        this.mContext = mContext;
        this.preferences = mContext.getSharedPreferences(Constant.NAME_SHAREDPREF, Context.MODE_PRIVATE);
        this.editor = preferences.edit();
    }

    // turn on/off service Control
    public boolean getStatusControl() {
        return preferences.getBoolean(Constant.KEY_STATUS_CONTROL, true);
    }

    public void setStatusControl(boolean status) {
        editor.putBoolean(Constant.KEY_STATUS_CONTROL, status).commit();
    }

    public ItemApplication getItemAppDataMore1() {
        String json = preferences.getString(Constant.ITEM_APP_1, null);
        Type type = new TypeToken<ItemApplication>() {
        }.getType();
        ItemApplication obj = new Gson().fromJson(json, type);
        if (obj != null) {
            return obj;
        } else {
            return new ItemApplication(false, Constant.NON_APP, Constant.NON_APP, 0);
        }
    }

    public void setItemAppDataMore1(ItemApplication obj) {
        String json = new Gson().toJson(obj);
        editor.putString(Constant.ITEM_APP_1, json);
        editor.commit();
    }

    public ItemApplication getItemAppDataMore2() {
        String json = preferences.getString(Constant.ITEM_APP_2, null);
        Type type = new TypeToken<ItemApplication>() {
        }.getType();
        ItemApplication obj = new Gson().fromJson(json, type);
        if (obj != null) {
            return obj;
        } else {
            return new ItemApplication(false, Constant.NON_APP, Constant.NON_APP, 0);
        }
    }

    public void setItemAppDataMore2(ItemApplication obj) {
        String json = new Gson().toJson(obj);
        editor.putString(Constant.ITEM_APP_2, json);
        editor.commit();
    }

    public ItemApplication getItemAppDataMore3() {
        String json = preferences.getString(Constant.ITEM_APP_3, null);
        Type type = new TypeToken<ItemApplication>() {
        }.getType();
        ItemApplication obj = new Gson().fromJson(json, type);
        if (obj != null) {
            return obj;
        } else {
            return new ItemApplication(false, Constant.NON_APP, Constant.NON_APP, 0);
        }
    }

    public void setItemAppDataMore3(ItemApplication obj) {
        String json = new Gson().toJson(obj);
        editor.putString(Constant.ITEM_APP_3, json);
        editor.commit();
    }

    public ItemApplication getItemAppDataMore4() {
        String json = preferences.getString(Constant.ITEM_APP_4, null);
        Type type = new TypeToken<ItemApplication>() {
        }.getType();
        ItemApplication obj = new Gson().fromJson(json, type);
        if (obj != null) {
            return obj;
        } else {
            return new ItemApplication(false, Constant.NON_APP, Constant.NON_APP, 0);
        }
    }

    public void setItemAppDataMore4(ItemApplication obj) {
        String json = new Gson().toJson(obj);
        editor.putString(Constant.ITEM_APP_4, json);
        editor.commit();
    }

    public String getSongName() {
        return preferences.getString(Constant.MUSIC_NAME, mContext.getString(R.string.str_default_music));
    }

    public void setSongName(String musicName) {
        editor.putString(Constant.MUSIC_NAME, musicName).commit();
    }

    public String getSingerName() {
        return preferences.getString(Constant.SINGER_NAME, "");
    }

    public void setSingerName(String musicName) {
        editor.putString(Constant.SINGER_NAME, musicName).commit();
    }

    public boolean getStatusPlay() {
        return preferences.getBoolean(Constant.STATUS_PLAY, false);
    }

    public void setStatusPlay(boolean musicName) {
        editor.putBoolean(Constant.STATUS_PLAY, musicName).commit();
    }

    public Bitmap getBitmapImage() {
        Bitmap bitmap = null;
        String previouslyEncodedImage = preferences.getString(Constant.BITMAP_IMAGE, "");

        if (!previouslyEncodedImage.equalsIgnoreCase("")) {
            byte[] b = Base64.decode(previouslyEncodedImage, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
        }
        return bitmap;
    }

    public void setBitmapImage(Bitmap bitmap) {
        String encodedImage = "";
        if (bitmap != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();

            encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        }
        editor.putString(Constant.BITMAP_IMAGE, encodedImage).commit();
    }


    public boolean getStatusVibration() {
        return preferences.getBoolean(Constant.STATUS_VIBRATION, false);
    }

    public void setStatusVibration(boolean b) {
        editor.putBoolean(Constant.STATUS_VIBRATION, b).commit();
    }

    public boolean getStatusTouch() {
        return preferences.getBoolean(Constant.STATUS_TOUCH, false);
    }

    public void setStatusTouch(boolean b) {
        editor.putBoolean(Constant.STATUS_TOUCH, b).commit();
    }

    public boolean getStatusShowNoti() {
        return preferences.getBoolean(Constant.STATUS_SHOW_NOTI, true);
    }

    public void setStatusShowNoti(boolean b) {
        editor.putBoolean(Constant.STATUS_SHOW_NOTI, b).commit();
    }

    public int getIndexBackground() {
        return preferences.getInt(Constant.INDEX_BACKGROUND, 2);
    }

    public void setIndexBackground(int i) {
        editor.putInt(Constant.INDEX_BACKGROUND, i).commit();
    }

    public int getSizeView() {
        return preferences.getInt(Constant.SIZE_VIEW, 150);
    }

    public void setSizeView(int i) {
        editor.putInt(Constant.SIZE_VIEW, i).commit();
    }

    public int getTemperature() {
        return preferences.getInt(Constant.TEMPERATURE, 20);
    }

    public void setTemperature(int i) {
        editor.putInt(Constant.TEMPERATURE, i).commit();
    }

    public void setIntensity(int value) {
        editor.putInt(Constant.INTENSITY, value).commit();
    }

    public int getIntensity() {
        return preferences.getInt(Constant.INTENSITY, 50);
    }

    public int getIndexPosition() {
        return preferences.getInt(Constant.INDEX_POSTION, 2);
    }

    public void setIndexPosition(int i) {
        editor.putInt(Constant.INDEX_POSTION, i).commit();
    }
}
