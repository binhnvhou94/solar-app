package com.controlcenter.icontrol.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.media.MediaMetadataRetriever;
import android.media.RemoteControlClient;
import android.media.RemoteController;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.provider.AlarmClock;
import android.provider.Settings;
import android.support.animation.DynamicAnimation;
import android.support.animation.SpringAnimation;
import android.support.annotation.RequiresApi;
import android.support.v7.app.NotificationCompat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.SeekBar;
import android.widget.TextView;

import com.controlcenter.icontrol.ActivityFavorite;
import com.controlcenter.icontrol.MainActivity;
import com.controlcenter.icontrol.R;
import com.controlcenter.icontrol.broadcast.ControlBroadcast;
import com.controlcenter.icontrol.object.ItemApplication;
import com.controlcenter.icontrol.othor.ApplicationMethor;
import com.controlcenter.icontrol.othor.Constant;
import com.controlcenter.icontrol.othor.SharedPrefControl;
import com.controlcenter.icontrol.othor.Utils;
import com.controlcenter.icontrol.view.ViewGroupControl;
import com.controlcenter.icontrol.view.ViewGroupControlDown;
import com.controlcenter.icontrol.view.ViewGroupControlLeft;
import com.controlcenter.icontrol.widgets.VerticalSeekBar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jp.wasabeef.blurry.Blurry;

public class ControlService extends Service implements View.OnTouchListener, View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private IntentFilter filter;
    private BroadcastReceiver broadcastActionClose;

    private LayoutInflater inflater;
    private ViewGroupControlDown mViewGroupControlDown;
    private ViewGroupControlLeft mViewGroupControlLeft;
    private WindowManager mWindow;
    private View mview;
    private WindowManager.LayoutParams params;
    private ControlBroadcast mControlBroadCast;
    private ImageView btn_control;
    private LinearLayout ll_iconControl;

    private Context mContext;
    private int height, width;

    //-----------------------------------------
    private SharedPrefControl sharedPrefControl;
    private View mViewFull;

    private ViewGroupControl mViewGroupControl;

    private ImageView img_bgr, img_airplane, img_data_mobile, img_wifi, img_bluetooth, img_orient, img_bother, img_flash, img_lockmode, img_calculator, img_camera;
    private RelativeLayout rl_fresh, rl_screentime, rl_screentime_full;
    private FrameLayout fl_main;
    private LinearLayout rl_main, ll_control_app_next;
    private TextView tv_15s, tv_30s, tv_1m, tv_2m, tv_10m, tv_30m;

    private ApplicationMethor methor;
    private boolean checkScreenTimeOut = false;

    private VerticalSeekBar sb_light, sb_volumn;
    private ImageView img_light, img_volumn;

    //music
    private ImageView img_music, img_previous, img_play, img_next;
    private TextView tv_song, tv_singer;
    private RelativeLayout rl_music_app;

    private ImageView img_1, img_2, img_3, img_4;
    private RelativeLayout rl_service;

    float lastY = 0, nowY = 0;
    private float lastX = 0;
    private float nowX = 0;


    @Override
    public void onCreate() {
        super.onCreate();
        mWindow = (WindowManager) getSystemService(WINDOW_SERVICE);
        mContext = this;


        height = getResources().getDisplayMetrics().heightPixels;
        width = getResources().getDisplayMetrics().widthPixels;

        sharedPrefControl = new SharedPrefControl(this);
        methor = new ApplicationMethor(this);

        //check show noti
        if (sharedPrefControl.getStatusShowNoti()) {
            showNoti();
        }

        initServiceDownFirst();
        initViewServiceDown();
        startBroadcastControl();

        checkNotificationAccessPermission();
    }


    private void initViewServiceDown() {
        ll_iconControl = mview.findViewById(R.id.ll_iconControl);
        btn_control = mview.findViewById(R.id.btn_control);

        ll_iconControl.setOnTouchListener(this);
        btn_control.setOnTouchListener(this);

        //size
        setupDefaultSize();

        //default color
        GradientDrawable drawable = (GradientDrawable) btn_control.getBackground();
        drawable.setColor(getcolor(sharedPrefControl.getTemperature()));

        //default apha
        float p = (float) (100 - sharedPrefControl.getIntensity()) / 100.0f;
        btn_control.setAlpha(p);
    }

    private void setupDefaultSize() {
        if (sharedPrefControl.getIndexPosition() == 2) {
            ViewGroup.LayoutParams params = btn_control.getLayoutParams();
            params.width = Utils.dpToPx(sharedPrefControl.getSizeView());
            btn_control.setLayoutParams(params);
        } else if (sharedPrefControl.getIndexPosition() == 1) {
            ViewGroup.LayoutParams params = btn_control.getLayoutParams();
            params.height = Utils.dpToPx(sharedPrefControl.getSizeView());
            btn_control.setLayoutParams(params);
        } else if (sharedPrefControl.getIndexPosition() == 0) {
            ViewGroup.LayoutParams params = btn_control.getLayoutParams();
            params.height = Utils.dpToPx(sharedPrefControl.getSizeView());
            btn_control.setLayoutParams(params);
        }
    }

    private void startBroadcastControl() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_BOOT_COMPLETED);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        mControlBroadCast = new ControlBroadcast();
        registerReceiver(mControlBroadCast, filter);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mview != null) {
            NotificationManager mNotificationManager = (NotificationManager)
                    getSystemService(NOTIFICATION_SERVICE);
            mNotificationManager.cancelAll();
            mWindow.removeView(mview);
            mview = null;
        }
        removeServiceControlFull();
        unregisterReceiver(mControlBroadCast);
    }

    private void removeServiceControlFull() {
        if (mViewFull != null) {

            if (broadcastActionClose != null) {
                mContext.unregisterReceiver(broadcastActionClose);
                broadcastActionClose = null;
            }

            if (mViewFull != null) {
                mWindow.removeView(mViewFull);
                methor.stopCamera();
                mViewFull = null;
                try {
                    setStopServiceMusic();
                } catch (Exception e) {

                }
            } else {
                methor.stopCamera();
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                if (!sharedPrefControl.getStatusTouch()) {
                    checkTouchActionUp(event);
                }
                break;

            case MotionEvent.ACTION_DOWN:
                if (sharedPrefControl.getStatusTouch()) {
                    displayControlService();
                } else {
                    checkTouchActionDown(event);
                    rl_fresh.setVisibility(View.GONE);
                }
                break;

            case MotionEvent.ACTION_MOVE:
                if (!sharedPrefControl.getStatusTouch()) {
                    checkTouchActionMove(event);
                }
                break;
        }
        return true;
    }

    private void checkTouchActionUp(MotionEvent event) {
        if (sharedPrefControl.getIndexPosition() == 2) {
            if (nowY <= lastY) {
                SpringAnimation springAnim = new SpringAnimation(rl_service, DynamicAnimation.Y, 0);
                springAnim.setStartVelocity(1000);
                springAnim.setStartValue(event.getRawY() - height + rl_main.getHeight());
                springAnim.animateToFinalPosition(0);
                rl_fresh.setVisibility(View.VISIBLE);
                startServiceFull();
            } else {
                SpringAnimation springAnim = new SpringAnimation(rl_service, DynamicAnimation.Y, 0);
                springAnim.setStartValue(event.getRawY());
                springAnim.animateToFinalPosition(height);
                removeServiceControlFull();
            }
        } else if (sharedPrefControl.getIndexPosition() == 1) {
            if (nowX <= lastX) {
                SpringAnimation springAnim = new SpringAnimation(rl_service, DynamicAnimation.X, 0);
                springAnim.setStartVelocity(1000);
                springAnim.setStartValue(event.getRawX());
                springAnim.animateToFinalPosition(0);
                rl_fresh.setVisibility(View.VISIBLE);
                startServiceFull();
            } else {
                SpringAnimation springAnim = new SpringAnimation(rl_service, DynamicAnimation.X, 0);
                springAnim.setStartValue(event.getRawX());
                springAnim.animateToFinalPosition(width);
                removeServiceControlFull();
            }
        } else if (sharedPrefControl.getIndexPosition() == 0) {
            if (nowX >= lastX) {
                SpringAnimation springAnim = new SpringAnimation(rl_service, DynamicAnimation.X, 0);
                springAnim.setStartVelocity(1000);
                springAnim.setStartValue(event.getRawX() - width);
                springAnim.animateToFinalPosition(0);
                rl_fresh.setVisibility(View.VISIBLE);
                startServiceFull();
            } else {
                SpringAnimation springAnim = new SpringAnimation(rl_service, DynamicAnimation.X, 0);
                springAnim.setStartValue(event.getRawX() - width);
                springAnim.animateToFinalPosition(0);
                removeServiceControlFull();
            }
        }
    }

    private void checkTouchActionMove(MotionEvent event) {
        if (sharedPrefControl.getIndexPosition() == 2) {
            lastY = nowY;
            nowY = event.getRawY();

            rl_service.setY(event.getRawY() - height + rl_main.getHeight());
        } else if (sharedPrefControl.getIndexPosition() == 1) {
            lastX = nowX;
            nowX = event.getRawX();
            rl_service.setX(event.getRawX());
        } else if (sharedPrefControl.getIndexPosition() == 0) {
            lastX = nowX;
            nowX = event.getRawX();
            rl_service.setX(event.getRawX() - width);
        }
    }

    private void checkTouchActionDown(MotionEvent event) {
        if (sharedPrefControl.getIndexPosition() == 2) {
            lastY = event.getRawY();
            nowY = event.getRawY();
            displayControlService();
        } else if (sharedPrefControl.getIndexPosition() == 1) {
            lastX = event.getRawX();
            nowX = event.getRawX();
            displayControlService();
        } else if (sharedPrefControl.getIndexPosition() == 0) {
            lastX = event.getRawX();
            nowX = event.getRawX();
            displayControlService();
        }
    }

    private void startServiceFull() {
        if (sharedPrefControl.getStatusVibration()) {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(500);
        }
        checkData();
        checkStatusControl();
        checkNotificationAccessPermission();
    }


    private void displayControlService() {
        initService();
        initViewService();
        startServiceFull();
    }

    private void checkData() {
        ItemApplication a1, a2, a3, a4;
        a1 = sharedPrefControl.getItemAppDataMore1();
        a2 = sharedPrefControl.getItemAppDataMore2();
        a3 = sharedPrefControl.getItemAppDataMore3();
        a4 = sharedPrefControl.getItemAppDataMore4();
        if (a1 == null && a2 == null && a3 == null && a4 == null) {
            ll_control_app_next.setVisibility(View.GONE);
        } else {
            if (a1.getmAppName().equals(Constant.NON_APP) && a2.getmAppName().equals(Constant.NON_APP) && a3.getmAppName().equals(Constant.NON_APP) && a4.getmAppName().equals(Constant.NON_APP)) {
                ll_control_app_next.setVisibility(View.GONE);
            } else {
                ll_control_app_next.setVisibility(View.VISIBLE);
                setIcon1();
                setIcon2();
                setIcon3();
                setIcon4();
            }
        }
    }

    private void setIcon4() {
        try {
            Drawable img = getPackageManager().getApplicationIcon(sharedPrefControl.getItemAppDataMore4().getmPacketName());
            img_4.setImageDrawable(img);
        } catch (Exception e) {
            img_4.setImageResource(R.drawable.ic_default);
            e.printStackTrace();
        }
    }

    private void setIcon3() {
        try {
            Drawable img = getPackageManager().getApplicationIcon(sharedPrefControl.getItemAppDataMore3().getmPacketName());
            img_3.setImageDrawable(img);
        } catch (Exception e) {
            img_3.setImageResource(R.drawable.ic_default);
            e.printStackTrace();
        }
    }

    private void setIcon2() {
        try {
            Drawable img = getPackageManager().getApplicationIcon(sharedPrefControl.getItemAppDataMore2().getmPacketName());
            img_2.setImageDrawable(img);
        } catch (Exception e) {
            img_2.setImageResource(R.drawable.ic_default);
            e.printStackTrace();
        }
    }

    private void setIcon1() {
        try {
            Drawable img = getPackageManager().getApplicationIcon(sharedPrefControl.getItemAppDataMore1().getmPacketName());
            img_1.setImageDrawable(img);
        } catch (Exception e) {
            img_1.setImageResource(R.drawable.ic_default);
            e.printStackTrace();
        }
    }

    private void checkStatusControl() {
        //check airplane
        methor.setAirMode(img_airplane, mContext);
        //data mobile
        methor.checkMobileDataEnable(img_data_mobile);
        //wifi
        methor.getStatusWifi(img_wifi);
        //bluetooth
        methor.setStatusBluetooth(img_bluetooth);
        //screen orientation
        methor.getStatusOrientation(img_orient);
        //bother - ngủ đêm
        methor.getStatusBother(img_bother);
        //brightness light
        methor.getProgressBarBrightness(sb_light, img_light);
        sb_light.setOnSeekBarChangeListener(this);
        //volumn
        methor.getSeekbarVolumn(sb_volumn, img_volumn);
        sb_volumn.setOnSeekBarChangeListener(this);
        //flashlight
//        methor.getStatusFlashLight(img_flash);

    }

    private void initService() {
        mViewGroupControl = new ViewGroupControl(this) {
            //home or recent button

            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                    removeServiceControlFull();
                }
                return super.dispatchKeyEvent(event);
            }
        };
        mViewGroupControl.setFocusable(true);

        inflater = LayoutInflater.from(this);
        mViewFull = inflater.inflate(R.layout.service_control, mViewGroupControl);
        mWindow = (WindowManager) getSystemService(WINDOW_SERVICE);
        params = new WindowManager.LayoutParams();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
        params.flags = WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                | WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        params.type = WindowManager.LayoutParams.TYPE_PHONE;
        params.format = PixelFormat.TRANSLUCENT;
        params.gravity = Gravity.TOP | Gravity.START;
        mWindow.addView(mViewFull, params);

        filter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        broadcastActionClose = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Do some things
                removeServiceControlFull();
            }
        };

        mContext.registerReceiver(broadcastActionClose, filter);

    }

    private void initViewService() {

        fl_main = mViewFull.findViewById(R.id.fl_main);

        rl_fresh = mViewFull.findViewById(R.id.rl_fresh);
        rl_screentime = mViewFull.findViewById(R.id.rl_screentime);
        img_bgr = mViewFull.findViewById(R.id.img_bgr);
        img_airplane = mViewFull.findViewById(R.id.img_airplane);
        img_data_mobile = mViewFull.findViewById(R.id.img_data_mobile);
        img_wifi = mViewFull.findViewById(R.id.img_wifi);
        img_bluetooth = mViewFull.findViewById(R.id.img_bluetooth);
        img_orient = mViewFull.findViewById(R.id.img_orient);
        img_bother = mViewFull.findViewById(R.id.img_bother);
        img_flash = mViewFull.findViewById(R.id.img_flash);
        img_lockmode = mViewFull.findViewById(R.id.img_lockmode);
        img_calculator = mViewFull.findViewById(R.id.img_calculator);
        img_camera = mViewFull.findViewById(R.id.img_camera);
        rl_screentime_full = mViewFull.findViewById(R.id.rl_screentime_full);
        rl_main = mViewFull.findViewById(R.id.rl_main);

        ll_control_app_next = mViewFull.findViewById(R.id.ll_control_app_next);

        rl_fresh.setOnClickListener(this);
        rl_screentime.setOnClickListener(this);
        img_airplane.setOnClickListener(this);
        img_data_mobile.setOnClickListener(this);
        img_wifi.setOnClickListener(this);
        img_bluetooth.setOnClickListener(this);
        img_orient.setOnClickListener(this);
        img_bother.setOnClickListener(this);
        img_flash.setOnClickListener(this);
        img_lockmode.setOnClickListener(this);
        img_calculator.setOnClickListener(this);
        img_camera.setOnClickListener(this);
        rl_screentime_full.setOnClickListener(this);

        // screen timeout
        tv_15s = mViewFull.findViewById(R.id.tv_15s);
        tv_30s = mViewFull.findViewById(R.id.tv_30s);
        tv_1m = mViewFull.findViewById(R.id.tv_1m);
        tv_2m = mViewFull.findViewById(R.id.tv_2m);
        tv_10m = mViewFull.findViewById(R.id.tv_10m);
        tv_30m = mViewFull.findViewById(R.id.tv_30m);
        tv_15s.setOnClickListener(this);
        tv_30s.setOnClickListener(this);
        tv_1m.setOnClickListener(this);
        tv_2m.setOnClickListener(this);
        tv_10m.setOnClickListener(this);
        tv_30m.setOnClickListener(this);

        //seekbar
        img_light = mViewFull.findViewById(R.id.img_light);
        sb_light = mViewFull.findViewById(R.id.sb_light);


        img_volumn = mViewFull.findViewById(R.id.img_volumn);
        sb_volumn = mViewFull.findViewById(R.id.sb_volumn);


        //image
        img_1 = mViewFull.findViewById(R.id.img_1);
        img_2 = mViewFull.findViewById(R.id.img_2);
        img_3 = mViewFull.findViewById(R.id.img_3);
        img_4 = mViewFull.findViewById(R.id.img_4);

        img_1.setOnClickListener(this);
        img_2.setOnClickListener(this);
        img_3.setOnClickListener(this);
        img_4.setOnClickListener(this);

        //music
        img_music = mViewFull.findViewById(R.id.img_music);
        img_previous = mViewFull.findViewById(R.id.img_previous);
        img_play = mViewFull.findViewById(R.id.img_play);
        img_next = mViewFull.findViewById(R.id.img_next);
        tv_song = mViewFull.findViewById(R.id.tv_song);
        tv_singer = mViewFull.findViewById(R.id.tv_singer);
        rl_music_app = mViewFull.findViewById(R.id.rl_music_app);

        img_music.setOnClickListener(this);
        img_previous.setOnClickListener(this);
        img_play.setOnClickListener(this);
        img_next.setOnClickListener(this);
        rl_music_app.setOnClickListener(this);

        tv_singer.setText(sharedPrefControl.getSingerName());
        tv_song.setText(sharedPrefControl.getSongName());
        if (sharedPrefControl.getBitmapImage() != null) {
            img_music.setImageBitmap(sharedPrefControl.getBitmapImage());
        } else {
            img_music.setImageResource(R.drawable.icon);
        }

        if (sharedPrefControl.getStatusPlay()) {
            img_play.setImageResource(R.drawable.ic_pause);
        } else {
            img_play.setImageResource(R.drawable.ic_play);
        }


        rl_service = mViewFull.findViewById(R.id.rl_service);

        switch (sharedPrefControl.getIndexBackground()) {
            case 0:
                img_bgr.setBackgroundColor(Color.TRANSPARENT);
                break;

            case 1:
                WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);
                final Drawable wallpaperDrawable = wallpaperManager.getDrawable();
                Blurry.with(mContext).from(drawableToBitmap(wallpaperDrawable)).into(img_bgr);
                break;

            case 2:
                img_bgr.setImageResource(R.drawable.bgr_default);
                break;

            case 3:
                img_bgr.setBackgroundResource(R.drawable.bgr_screen);
                img_bgr.setImageAlpha(30);
                break;
        }

    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.sb_light:
                methor.setBrightness(progress, img_light);
                break;

            case R.id.sb_volumn:
                methor.setVolume(progress, img_volumn);
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_fresh:
                removeServiceControlFull();
                break;

            case R.id.img_airplane:
                methor.setFlightMode(mContext);
                methor.setAirMode(img_airplane, mContext);
                removeServiceControlFull();
                break;

            case R.id.img_data_mobile:
                methor.dataMobile();
                removeServiceControlFull();
                break;

            case R.id.img_wifi:
                methor.wifisettingMode(img_wifi);
                break;

            case R.id.img_bluetooth:
                methor.bluetoothSettingMode(img_bluetooth);
                break;

            case R.id.img_orient:
                methor.ScreenSettingOrtation(img_orient);
                break;

            case R.id.img_bother:
                methor.setupBother(img_bother);
                break;

            case R.id.img_flash:
                setingFlashLight();
                break;

            case R.id.img_lockmode:
                settingAlarmManager();
                break;

            case R.id.img_calculator:
                settingCalculator();
                break;

            case R.id.img_camera:
                settingCamera();
                break;

            case R.id.rl_screentime:
                checkScreenTimeOut = !checkScreenTimeOut;
                displayScreenTimeFull(checkScreenTimeOut);
                break;

            case R.id.rl_screentime_full:
                checkScreenTimeOut = !checkScreenTimeOut;
                displayScreenTimeFull(checkScreenTimeOut);
                break;

            case R.id.tv_15s:
                checkScreenTimeOut = !checkScreenTimeOut;
                methor.setupScreenTimeout(15000);
                displayScreenTimeFull(false);
                break;

            case R.id.tv_30s:
                checkScreenTimeOut = !checkScreenTimeOut;
                methor.setupScreenTimeout(30000);
                displayScreenTimeFull(false);
                break;

            case R.id.tv_1m:
                checkScreenTimeOut = !checkScreenTimeOut;
                methor.setupScreenTimeout(60000);
                displayScreenTimeFull(false);
                break;

            case R.id.tv_2m:
                checkScreenTimeOut = !checkScreenTimeOut;
                methor.setupScreenTimeout(120000);
                displayScreenTimeFull(false);
                break;

            case R.id.tv_10m:
                checkScreenTimeOut = !checkScreenTimeOut;
                methor.setupScreenTimeout(600000);
                displayScreenTimeFull(false);
                break;

            case R.id.tv_30m:
                checkScreenTimeOut = !checkScreenTimeOut;
                methor.setupScreenTimeout(1800000);
                displayScreenTimeFull(false);
                break;

            case R.id.img_play:
                if (mBound) {
                    if (mIsPlaying) {
                        mRCService.sendPauseKey();
                    } else {
                        mRCService.sendPlayKey();
                    }
                }
                break;

            case R.id.img_next:
                if (mBound) {
                    mRCService.sendNextKey();
                }
                break;

            case R.id.img_previous:
                if (mBound) {
                    mRCService.sendPreviousKey();
                }
                break;

            case R.id.img_1:
                if (sharedPrefControl.getItemAppDataMore1() != null) {
                    callRunning(sharedPrefControl.getItemAppDataMore1().getmPacketName());
                } else {
                    Intent i = new Intent(this, ActivityFavorite.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    removeServiceControlFull();
                }
                break;

            case R.id.img_2:
                if (sharedPrefControl.getItemAppDataMore2() != null) {
                    callRunning(sharedPrefControl.getItemAppDataMore2().getmPacketName());
                } else {
                    Intent i = new Intent(this, ActivityFavorite.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    removeServiceControlFull();
                }
                break;

            case R.id.img_3:
                if (sharedPrefControl.getItemAppDataMore3() != null) {
                    callRunning(sharedPrefControl.getItemAppDataMore3().getmPacketName());
                } else {
                    Intent i = new Intent(this, ActivityFavorite.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    removeServiceControlFull();
                }
                break;

            case R.id.img_4:
                if (sharedPrefControl.getItemAppDataMore4() != null) {
                    callRunning(sharedPrefControl.getItemAppDataMore4().getmPacketName());
                } else {
                    Intent i = new Intent(this, ActivityFavorite.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    removeServiceControlFull();
                }
                break;

        }
    }

    private void callRunning(String appName) {
        if (appName.equals(Constant.NON_APP)) {
            Intent i = new Intent(this, ActivityFavorite.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        } else {
            PackageManager pm = mContext.getPackageManager();
            Intent launchIntent = pm.getLaunchIntentForPackage(appName);
            launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(launchIntent);
        }
        removeServiceControlFull();
    }

    private void settingCamera() {
        methor.stopCamera();
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(cameraIntent);
        removeServiceControlFull();
    }

    private void settingCalculator() {
        try {
            Intent intent = new Intent();
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_APP_CALCULATOR);
            mContext.startActivity(intent);
        } catch (Exception e) {
            ArrayList<HashMap<String, Object>> items = new ArrayList<HashMap<String, Object>>();
            PackageManager pm;
            pm = mContext.getPackageManager();
            List<PackageInfo> packs = pm.getInstalledPackages(0);
            for (PackageInfo pi : packs) {
                if (pi.packageName.toString().toLowerCase().contains("calcul")) {
                    HashMap<String, Object> map = new HashMap<>();
                    map.put("appName", pi.applicationInfo.loadLabel(pm));
                    map.put("packageName", pi.packageName);
                    items.add(map);
                }
            }
            if (items.size() >= 1) {
                String packageName = (String) items.get(0).get("packageName");
                Intent i = pm.getLaunchIntentForPackage(packageName);
                if (i != null)
                    mContext.startActivity(i);
            }
        }
        removeServiceControlFull();
    }

    private void settingAlarmManager() {
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
        removeServiceControlFull();
    }

    private void setingFlashLight() {
        methor.getCamera();
        methor.checkflashsystem();
        methor.checkflash(img_flash);
    }

    private void displayScreenTimeFull(boolean check) {
        if (check) {
            rl_screentime_full.setVisibility(View.VISIBLE);
            rl_main.setVisibility(View.INVISIBLE);
        } else {
            rl_screentime_full.setVisibility(View.GONE);
            rl_main.setVisibility(View.VISIBLE);
        }
    }

    private void setupMusic() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                Intent intent = new Intent("com.controlcenter.icontrol.BIND_RC_CONTROL_SERVICE");
                intent.setPackage("com.controlcenter.icontrol");
                intent.setAction("com.controlcenter.icontrol.BIND_RC_CONTROL_SERVICE");
                mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            } else {
                Intent intent = new Intent("com.controlcenter.icontrol.BIND_RC_CONTROL_SERVICE");
                mContext.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mClientUpdateListener = new RemoteController.OnClientUpdateListener() {
                private boolean mScrubbingSupported = false;

                private boolean isScrubbingSupported(int flags) {
                    return (flags & RemoteControlClient.FLAG_KEY_MEDIA_POSITION_UPDATE) != 0;
                }

                @Override
                public void onClientTransportControlUpdate(int transportControlFlags) {
                    mScrubbingSupported = isScrubbingSupported(transportControlFlags);
                    if (mScrubbingSupported) {
//                        mScrubBar.setEnabled(true);
//                        mHandler.post(mUpdateSeekBar);
                    } else {
//                        mScrubBar.setEnabled(false);
//                        mHandler.removeCallbacks(mUpdateSeekBar);
                    }
                }

                @Override
                public void onClientPlaybackStateUpdate(int state, long stateChangeTimeMs, long currentPosMs, float speed) {
                    switch (state) {
                        case RemoteControlClient.PLAYSTATE_PLAYING:
                            if (mScrubbingSupported)
//                                mHandler.post(mUpdateSeekBar);
                                mIsPlaying = true;

                            if (img_play != null)
                                img_play.setImageResource(R.drawable.ic_pause);
                            break;
                        default:
//                            mHandler.removeCallbacks(mUpdateSeekBar);
                            mIsPlaying = false;
                            if (img_play != null)
                                img_play.setImageResource(R.drawable.ic_play);
                            break;
                    }
                    sharedPrefControl.setStatusPlay(mIsPlaying);
//                    mScrubBar.setProgress((int) (currentPosMs * mScrubBar.getMax() / mSongDuration));

                }

                @Override
                public void onClientPlaybackStateUpdate(int state) {
                    switch (state) {
                        case RemoteControlClient.PLAYSTATE_PLAYING:
                            if (mScrubbingSupported)
//                                mHandler.post(mUpdateSeekBar);
                                mIsPlaying = true;
                            if (img_play != null)
                                img_play.setImageResource(R.drawable.ic_pause);
                            break;
                        default:
//                            mHandler.removeCallbacks(mUpdateSeekBar);
                            mIsPlaying = false;
                            if (img_play != null)
                                img_play.setImageResource(R.drawable.ic_play);
                            break;
                    }
                    sharedPrefControl.setStatusPlay(mIsPlaying);
                }

                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onClientMetadataUpdate(RemoteController.MetadataEditor editor) {
                    String name_song = editor.getString(MediaMetadataRetriever.METADATA_KEY_TITLE, mContext.getString(R.string.app_music));
                    String name_singer = editor.getString(MediaMetadataRetriever.METADATA_KEY_ARTIST, "");
                    Bitmap bitmap = editor.getBitmap(RemoteController.MetadataEditor.BITMAP_KEY_ARTWORK, null);
                    if (tv_song != null) {
                        tv_song.setText(name_song);
                        tv_singer.setText(name_singer);

                        mSongDuration = editor.getLong(MediaMetadataRetriever.METADATA_KEY_DURATION, 1);
                        img_music.setImageBitmap(bitmap);
                    }
                    sharedPrefControl.setSongName(name_song);
                    sharedPrefControl.setSingerName(name_singer);
                    sharedPrefControl.setBitmapImage(bitmap);
                }

                @Override
                public void onClientChange(boolean clearing) {

                }
            };
        }

    }

    public void setStopServiceMusic() {
        try {
            if (mBound) {
                mRCService.setRemoteControllerDisabled();
            }
            mContext.unbindService(mConnection);
        } catch (Exception e) {
        }
    }

    private void checkNotificationAccessPermission() {
        if (Settings.Secure.getString(this.getContentResolver(), "enabled_notification_listeners") != null)
            if (Settings.Secure.getString(this.getContentResolver(), "enabled_notification_listeners").contains(getApplicationContext().getPackageName())) {
                //service is enabled do something
                setupMusic();
            }
    }


    //Control music
    protected RemoteControlService mRCService;
    protected boolean mBound = false;
    protected Handler mHandler = new Handler();
    protected boolean mIsPlaying = false;
    protected long mSongDuration = 1;
    ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // Getting the binder and activating RemoteController instantly
            RemoteControlService.RCBinder binder = (RemoteControlService.RCBinder) service;
            mRCService = binder.getService();
            mRCService.setRemoteControllerEnabled();
            mRCService.setClientUpdateListener(mClientUpdateListener);
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBound = false;
        }
    };
    RemoteController.OnClientUpdateListener mClientUpdateListener;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null && intent.getStringExtra(Constant.ACCESS_SHOW_NOTI) != null) {
            if (intent.getStringExtra(Constant.ACCESS_SHOW_NOTI).equals(Constant.ACCESS_SHOW_NOTI)) {
                if (intent.getBooleanExtra(Constant.PUT_SHOW_NOTI, true)) {
                    showNoti();
                } else {
                    NotificationManager mNotificationManager = (NotificationManager)
                            getSystemService(NOTIFICATION_SERVICE);
                    mNotificationManager.cancelAll();
                }
            }
        }

        if (intent != null && intent.getStringExtra(Constant.CHANGE_SIZE) != null) {
            if (intent.getStringExtra(Constant.CHANGE_SIZE).equals(Constant.CHANGE_SIZE)) {
                setupDefaultSize();
            }
        }

        if (intent != null && intent.getStringExtra(Constant.TEMPERATURE_VIEW) != null) {
            if (intent.getStringExtra(Constant.TEMPERATURE_VIEW).equals(Constant.TEMPERATURE_VIEW)) {
                GradientDrawable drawable = (GradientDrawable) btn_control.getBackground();
                drawable.setColor(getcolor(sharedPrefControl.getTemperature()));
            }
        }

        if (intent != null && intent.getStringExtra(Constant.INTENSITY_VIEW) != null) {
            if (intent.getStringExtra(Constant.INTENSITY_VIEW).equals(Constant.INTENSITY_VIEW)) {
                float p = (float) (100 - sharedPrefControl.getIntensity()) / 100.0f;
                btn_control.setAlpha(p);
            }
        }

        if (intent != null && intent.getStringExtra(Constant.CHANGE_POSITION) != null) {
            if (intent.getStringExtra(Constant.CHANGE_POSITION).equals(Constant.CHANGE_POSITION)) {
                initServiceDownFirst();
            }
        }

        return START_STICKY;
    }

    private void initServiceDownFirst() {
        if (mViewGroupControlDown != null) {
            mWindow.removeView(mViewGroupControlDown);
            mViewGroupControlDown = null;
        }
        if (mViewGroupControlLeft != null) {
            mWindow.removeView(mViewGroupControlLeft);
            mViewGroupControlLeft = null;
        }


        if (sharedPrefControl.getIndexPosition() == 2) {
            mViewGroupControlDown = new ViewGroupControlDown(this);
            inflater = LayoutInflater.from(this);
            mview = inflater.inflate(R.layout.service_control_down_first, mViewGroupControlDown);
            params = new WindowManager.LayoutParams();
            params.screenOrientation = Configuration.ORIENTATION_PORTRAIT;
            params.gravity = Gravity.BOTTOM;
            params.width = WindowManager.LayoutParams.MATCH_PARENT;
            params.height = WindowManager.LayoutParams.WRAP_CONTENT;
            params.flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                    | WindowManager.LayoutParams.FIRST_SUB_WINDOW;
            params.type = WindowManager.LayoutParams.TYPE_PHONE;
            params.format = PixelFormat.TRANSLUCENT;
            mWindow.addView(mview, params);

            initViewServiceDown();
            startBroadcastControl();

            checkNotificationAccessPermission();
        } else if (sharedPrefControl.getIndexPosition() == 1) {
            mViewGroupControlLeft = new ViewGroupControlLeft(mContext);
            inflater = LayoutInflater.from(this);
            mview = inflater.inflate(R.layout.service_control_right_first, mViewGroupControlLeft);
            params = new WindowManager.LayoutParams();
            params.screenOrientation = Configuration.ORIENTATION_PORTRAIT;
            params.gravity = Gravity.RIGHT;
            params.width = WindowManager.LayoutParams.WRAP_CONTENT;
            params.height = WindowManager.LayoutParams.MATCH_PARENT;
            params.flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                    | WindowManager.LayoutParams.FIRST_SUB_WINDOW;
            params.type = WindowManager.LayoutParams.TYPE_PHONE;
            params.format = PixelFormat.TRANSLUCENT;
            mWindow.addView(mview, params);
            initViewServiceDown();
            startBroadcastControl();

            checkNotificationAccessPermission();
        } else if (sharedPrefControl.getIndexPosition() == 0) {
            mViewGroupControlLeft = new ViewGroupControlLeft(mContext);
            inflater = LayoutInflater.from(this);
            mview = inflater.inflate(R.layout.service_control_left_first, mViewGroupControlLeft);
            params = new WindowManager.LayoutParams();
            params.screenOrientation = Configuration.ORIENTATION_PORTRAIT;
            params.gravity = Gravity.LEFT;
            params.width = WindowManager.LayoutParams.WRAP_CONTENT;
            params.height = WindowManager.LayoutParams.MATCH_PARENT;
            params.flags = WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                    | WindowManager.LayoutParams.FIRST_SUB_WINDOW;
            params.type = WindowManager.LayoutParams.TYPE_PHONE;
            params.format = PixelFormat.TRANSLUCENT;
            mWindow.addView(mview, params);
            initViewServiceDown();
            startBroadcastControl();

            checkNotificationAccessPermission();
        }

    }

    private int getcolor(float percent) {
        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.bitmkapcolor);
        float pixel = ((icon.getWidth() * percent)) / 100;
        if (pixel >= icon.getWidth()) {
            pixel = icon.getWidth() - 1;
        }
        int xxx = icon.getPixel((int) pixel, ((icon.getHeight() / 2)));
        return xxx;
    }

    private void showNoti() {
        Intent var4 = new Intent(mContext, MainActivity.class);
        var4.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 100, var4, PendingIntent.FLAG_UPDATE_CURRENT);
        RemoteViews contentView = new RemoteViews(getPackageName(), R.layout.custom_push);

        contentView.setImageViewResource(R.id.img_icon, R.drawable.icon);
        contentView.setTextViewText(R.id.tv_title, "Control Center");
        contentView.setTextViewText(R.id.tv_des, "Open Application");

        android.support.v4.app.NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.icon)
                .setContent(contentView);

        Notification notification = mBuilder.build();
        notification.contentIntent = pendingIntent;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.flags |= Notification.FLAG_NO_CLEAR;
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);

    }


}

