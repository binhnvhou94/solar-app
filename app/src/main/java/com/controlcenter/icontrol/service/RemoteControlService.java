package com.controlcenter.icontrol.service;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.RemoteController;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import android.view.KeyEvent;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by Cheng Lee on 9/29/2017.
 */
@SuppressWarnings("deprecation")
@TargetApi(Build.VERSION_CODES.KITKAT)
public class RemoteControlService extends NotificationListenerService implements RemoteController.OnClientUpdateListener {

    private static final int BITMAP_HEIGHT = 1024;
    private static final int BITMAP_WIDTH = 1024;

    private IBinder mBinder = new RCBinder();

    private RemoteController mRemoteController;
    private Context mContext;

    private RemoteController.OnClientUpdateListener mExternalClientUpdateListener;

    @Override
    public IBinder onBind(Intent intent) {
        if (intent.getAction().equals("com.controlcenter.icontrol.BIND_RC_CONTROL_SERVICE")) {
            return mBinder;
        } else {
            return super.onBind(intent);
        }
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {

    }

    @Override
    public void onNotificationRemoved(StatusBarNotification notification) {
    }

    @Override
    public void onCreate() {
        mContext = getApplicationContext();
        mRemoteController = new RemoteController(mContext, this);
    }

    @Override
    public void onDestroy() {
        setRemoteControllerDisabled();
    }

    public void setRemoteControllerEnabled() {

        if (!((AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE)).registerRemoteController(mRemoteController)) {
            try {
                throw new RuntimeException("Error while registering RemoteController!");
            } catch (Exception e) {
            }
        } else {
            mRemoteController.setArtworkConfiguration(BITMAP_WIDTH, BITMAP_HEIGHT);
            setSynchronizationMode(mRemoteController, RemoteController.POSITION_SYNCHRONIZATION_CHECK);
        }
    }

    public void setRemoteControllerDisabled() {
        ((AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE)).unregisterRemoteController(mRemoteController);
    }

    public void setClientUpdateListener(RemoteController.OnClientUpdateListener listener) {
        mExternalClientUpdateListener = listener;
    }

    public void sendNextKey() {
        sendKeyEvent(KeyEvent.KEYCODE_MEDIA_NEXT);
    }

    public void sendPreviousKey() {
        sendKeyEvent(KeyEvent.KEYCODE_MEDIA_PREVIOUS);
    }

    public void sendPauseKey() {
        if (!sendKeyEvent(KeyEvent.KEYCODE_MEDIA_PAUSE)) {
            sendKeyEvent(KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE);
        }
    }

    public void sendPlayKey() {
        if (!sendKeyEvent(KeyEvent.KEYCODE_MEDIA_PLAY)) {
            sendKeyEvent(KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE);
        }
    }

    public long getEstimatedPosition() {
        return mRemoteController.getEstimatedMediaPosition();
    }


    public void seekTo(long ms) {
        mRemoteController.seekTo(ms);
    }

    private void setSynchronizationMode(RemoteController controller, int sync) {
        if ((sync != RemoteController.POSITION_SYNCHRONIZATION_NONE) && (sync != RemoteController.POSITION_SYNCHRONIZATION_CHECK)) {
            throw new IllegalArgumentException("Unknown synchronization mode " + sync);
        }

        Class<?> iRemoteControlDisplayClass;
        Object rcDisplay;
        Field rcDisplayField;
        AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        try {
            iRemoteControlDisplayClass = Class.forName("android.media.IRemoteControlDisplay");
            Method remoteControlDisplayWantsPlaybackPositionSyncMethod;
            remoteControlDisplayWantsPlaybackPositionSyncMethod = AudioManager.class.getDeclaredMethod("remoteControlDisplayWantsPlaybackPositionSync", iRemoteControlDisplayClass, boolean.class);
            remoteControlDisplayWantsPlaybackPositionSyncMethod.setAccessible(true);
            rcDisplayField = RemoteController.class.getDeclaredField("mRcd");
            rcDisplayField.setAccessible(true);
            rcDisplay = rcDisplayField.get(mRemoteController);
            remoteControlDisplayWantsPlaybackPositionSyncMethod.invoke(am, iRemoteControlDisplayClass.cast(rcDisplay), true);
        } catch (ClassNotFoundException e1) {
            Log.e("Bibo", "setSynchronizationMode: " + e1.getException() );
//            throw new RuntimeException("Class IRemoteControlDisplay doesn't exist, can't access it with reflection");
        } catch (NoSuchMethodException e) {
//            throw new RuntimeException("Method remoteControlDisplayWantsPlaybackPositionSync() doesn't exist, can't access it with reflection");
        }catch (NoSuchFieldException e) {
//            throw new RuntimeException("Field mRcd doesn't exist, can't access it with reflection");
        } catch (IllegalAccessException e) {
//            throw new RuntimeException("Field mRcd can't be accessed - access denied");
        } catch (IllegalArgumentException e) {
//            throw new RuntimeException("Method remoteControlDisplayWantsPlaybackPositionSync() invocation failure - invalid arguments");
        } catch (InvocationTargetException e) {
//            throw new RuntimeException("Method remoteControlDisplayWantsPlaybackPositionSync() invocation failure - invalid invocation target");
        }

    }


    private boolean sendKeyEvent(int keyCode) {
        KeyEvent keyEvent = new KeyEvent(KeyEvent.ACTION_DOWN, keyCode);
        boolean first = mRemoteController.sendMediaKeyEvent(keyEvent);
        keyEvent = new KeyEvent(KeyEvent.ACTION_UP, keyCode);
        boolean second = mRemoteController.sendMediaKeyEvent(keyEvent);

        return first && second;
    }


    //the most simple Binder implementation
    public class RCBinder extends Binder {
        public RemoteControlService getService() {
            return RemoteControlService.this;
        }
    }

    @Override
    public void onClientChange(boolean arg0) {
        if (mExternalClientUpdateListener != null) {
            mExternalClientUpdateListener.onClientChange(arg0);
        }
    }

    @Override
    public void onClientMetadataUpdate(RemoteController.MetadataEditor arg0) {
        if (mExternalClientUpdateListener != null) {
            mExternalClientUpdateListener.onClientMetadataUpdate(arg0);
        }
    }

    @Override
    public void onClientPlaybackStateUpdate(int arg0) {
        if (mExternalClientUpdateListener != null) {
            mExternalClientUpdateListener.onClientPlaybackStateUpdate(arg0);
        }
    }

    @Override
    public void onClientPlaybackStateUpdate(int arg0, long arg1, long arg2, float arg3) {
        if (mExternalClientUpdateListener != null) {
            mExternalClientUpdateListener.onClientPlaybackStateUpdate(arg0, arg1, arg2, arg3);
        }
    }

    @Override
    public void onClientTransportControlUpdate(int arg0) {
        if (mExternalClientUpdateListener != null) {
            mExternalClientUpdateListener.onClientTransportControlUpdate(arg0);
        }

    }

}
