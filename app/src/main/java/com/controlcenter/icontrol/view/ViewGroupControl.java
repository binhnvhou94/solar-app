package com.controlcenter.icontrol.view;

import android.content.Context;
import android.widget.RelativeLayout;

public class ViewGroupControl extends RelativeLayout {

    public ViewGroupControl(Context context) {
        super(context);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    }

}
