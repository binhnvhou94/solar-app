package com.controlcenter.icontrol.view;

import android.content.Context;
import android.widget.RelativeLayout;

/**
 * Created by Cheng Lee on 9/25/2017.
 */

public class ViewGroupControlDown extends RelativeLayout {

    public ViewGroupControlDown(Context context) {
        super(context);
        setLayoutParams(new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
    }

}
