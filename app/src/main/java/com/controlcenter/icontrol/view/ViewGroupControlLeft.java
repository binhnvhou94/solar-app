package com.controlcenter.icontrol.view;

import android.content.Context;
import android.widget.RelativeLayout;

/**
 * Created by Cheng Lee on 9/25/2017.
 */

public class ViewGroupControlLeft extends RelativeLayout {

    public ViewGroupControlLeft(Context context) {
        super(context);
        setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT , LayoutParams.MATCH_PARENT));
    }

}
